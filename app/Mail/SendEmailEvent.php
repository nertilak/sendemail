<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendEmailEvent extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public  string $text;
    public  string $date;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(string $text,string $date)
    {
        $this->text = $text;
        $this->date = $date;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.test') ->with(['text'=>$this->text,'date'=>$this->date]);
    }
}
