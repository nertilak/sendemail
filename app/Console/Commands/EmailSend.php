<?php

namespace App\Console\Commands;

use App\Mail\SendEmailEvent;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class EmailSend extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:send {email} {text} {date}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->sendEmail();
        return 0;
    }

    public function sendEmail()
    {
        $this->line("-----------------------");
        $this->info("Send Email:");

        $email = $this->argument('email');
        $text = $this->argument('text');
        $date = $this->argument('date');

        if(!$email) {
        $this->info("Email is required");
            
        }

        Mail::to($email)->send(new SendEmailEvent($text,$date));

        $this->info("Email send successfully");
    }
}
