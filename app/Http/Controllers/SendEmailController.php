<?php

namespace App\Http\Controllers;

use App\Http\Requests\SendEmailRequest;
use App\Mail\SendEmailEvent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class SendEmailController extends Controller
{
    public function index()
    {
        return view('send_email');
    }

    public function sendEmail(SendEmailRequest $request)
    {
        Mail::to($request->email)->send(new SendEmailEvent($request->text,$request->date));
        
        return view('send_email');
    }
}
