
<h1>Send Email</h1>

@if ($errors->any())
     @foreach ($errors->all() as $error)
         <div style="color:red">{{$error}}</div>
     @endforeach
@endif

{{Form::open (array ('url' => 'mail'))}}

<p> {{Form::text ('email', "", array ('placeholder'=>'Email','maxlength'=>30))}} </p>

<p> {{Form::text ('text',"", array('placeholder'=>'Text','maxlength'=>230)) }} </p>

<p> {{Form::text ('date',"", array('placeholder'=>'Date','maxlength'=>230)) }} </p>

<p> {{Form::submit ('Submit')}} </p>

{{Form::close ()}}